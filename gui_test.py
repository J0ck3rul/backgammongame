import gui
import pygame
import time

pygame.init()

gameTest = gui.Gui()

# player 1 e negru
old_player1 = [[1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 5], [7, 0], [8, 3], [9, 0], [10, 0], [11, 0], [12, 0], [
    13, 5], [14, 0], [15, 0], [16, 0], [17, 0], [18, 0], [19, 0], [20, 0], [21, 0], [22, 0], [23, 0], [24, 2]]
# player 2 e alb
old_player2 = [[1, 2], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0], [
    12, 5], [13, 0], [14, 0], [15, 0], [16, 0], [17, 3], [18, 0], [19, 5], [20, 0], [21, 0], [22, 0], [23, 0], [24, 0]]

def initializeBoard(piecesP1, piecesP2):
    player = "Black"
    for index, pieceStack in enumerate(piecesP1):
        for i in range(1, pieceStack[1]+1):
            # print pieces
            gameTest.place_piece(player,  (pieceStack[0], i)) # print

    player = "White"
    for index, pieceStack in enumerate(piecesP2):
        for i in range(1, pieceStack[1]+1):
            gameTest.place_piece(player,  (pieceStack[0], i)) # print

    # time.sleep(2)
    removeAllPieces(piecesP1, piecesP2)
    # time.sleep(10)

def printNewPieces(piecesP1, piecesP2):
    global old_player1
    global old_player2

    player = "Black"
    for index,pieceStack in enumerate(piecesP1):
        for i in range(1, pieceStack[1]+1):
            # print pieces
            gameTest.move(player,(0,0), (pieceStack[0], i)) # print

    player = "White"
    for index, pieceStack in enumerate(piecesP2):
        for i in range(1, pieceStack[1] + 1):
            gameTest.move(player, (0,0),(pieceStack[0], i)) # print

    old_player1 = piecesP1
    old_player2 = piecesP2

def removeAllPieces(piecesP1, piecesP2):
    for index, pieceStack in enumerate(piecesP1):
        for i in range(1, pieceStack[1] + 1):  # indexez de la 1
            gameTest.remove((pieceStack[0],i))
    
    for index, pieceStack in enumerate(piecesP2):
        for i in range(1, pieceStack[1] + 1):  # indexez de la 1
            gameTest.remove((pieceStack[0],i))