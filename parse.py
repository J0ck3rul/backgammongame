import os
import main
import gui
import gui_test
import time
from pyswip import Prolog, Query

CWD = os.path.dirname(__file__)
rounds_filepath = os.path.join(CWD, "rounds2.txt")

table_player1 = [[1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 5], [7, 0], [8, 3], [9, 0], [10, 0], [11, 0], [12, 0], [
    13, 5], [14, 0], [15, 0], [16, 0], [17, 0], [18, 0], [19, 0], [20, 0], [21, 0], [22, 0], [23, 0], [24, 2]]
table_player2 = [[1, 2], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0], [
    12, 5], [13, 0], [14, 0], [15, 0], [16, 0], [17, 3], [18, 0], [19, 5], [20, 0], [21, 0], [22, 0], [23, 0], [24, 0]]


# player 1 e negru
old_player1 = [[1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 5], [7, 0], [8, 3], [9, 0], [10, 0], [11, 0], [12, 0], [
    13, 5], [14, 0], [15, 0], [16, 0], [17, 0], [18, 0], [19, 0], [20, 0], [21, 0], [22, 0], [23, 0], [24, 2]]
# player 2 e alb
old_player2 = [[1, 2], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0], [
    12, 5], [13, 0], [14, 0], [15, 0], [16, 0], [17, 3], [18, 0], [19, 5], [20, 0], [21, 0], [22, 0], [23, 0], [24, 0]]

errorMove = False


def addPlayer1(move_to):
    global table_player1
    table_player1[move_to - 1][1] += 1


def decPlayer1(move_from):
    global table_player1
    table_player1[move_from - 1][1] -= 1


def addPlayer2(move_to):
    global table_player2
    table_player2[24 - move_to][1] += 1


def decPlayer2(move_from):
    global table_player2
    table_player2[24 - move_from][1] -= 1


def parse(filepath):
    global table_player1
    global table_player2

    global old_player1
    global old_player2

    global errorMove
    gui_test.initializeBoard(table_player1, table_player2)
    with open(filepath) as file:
        for line in file:
            if ("Wins" in line[0:32]):
                print("P1 wins")
                return
            if("Wins" in line[32:]):
                print("P2 wins")
                return 
            moves = line.split(';')
            player1_moves = moves[0].split(':')[1]
            player2_moves = moves[1].split(':')[1]
            p1_move_from = 0
            p1_move_to = 0
            winner = ""

            player1_dice = [
                int(int(moves[0].split(':')[0]) / 10), int(moves[0].split(':')[0]) % 10]
            player2_dice = [
                int(int(moves[1].split(':')[0]) / 10), int(moves[1].split(':')[0]) % 10]

            # Moves for player 1
            if player1_moves.strip():
                p1_moves_set = [x for x in player1_moves.split()]
                for i in range(len(p1_moves_set)):
                    if 'Bar' in p1_moves_set[i].split('/')[0]:
                        p1_move_from = 25
                    else:
                        p1_move_from = int(p1_moves_set[i].split('/')[0])

                    if '*' in p1_moves_set[i].split('/')[1]:
                        p1_move_to = int(p1_moves_set[i].split('/')[1][:-1])
                        table_player2[p1_move_to - 1][1] -= 1
                    else:
                        p1_move_to = int(p1_moves_set[i].split('/')[1])

                    if(main.isDiceMoveValidForOne(player1_dice[0], p1_move_to, p1_move_from) == True):
                        errorMove = False
                    elif(main.isDiceMoveValidForOne(player1_dice[1], p1_move_to, p1_move_from) == True):
                        errorMove = False
                    elif(main.isDiceMoveValidForSum(player1_dice, p1_move_to, p1_move_from) == True):
                        errorMove = False
                    else:
                        errorMove = True
                        print("Invalid move")
                        if(player1_dice[1] == player1_dice[0]):
                            continue
                        exit(0)


                    if(main.checkPosibleMove(p1_move_to, table_player2) == True):
                        print("Move is valid")
                        if(p1_move_from != 25 and p1_move_to):
                            addPlayer1(p1_move_to)
                            decPlayer1(p1_move_from)
                        elif(p1_move_from != 25 and p1_move_to == 0):
                            decPlayer1(p1_move_from)
                        elif(p1_move_from == 25 and p1_move_to):
                            addPlayer1(p1_move_to)
                        elif(p1_move_from == 25 and p1_move_to == 0):
                            print("Error")
                            exit(0)
                    elif (((p1_move_from - p1_move_to) <= player1_dice[0] or (p1_move_from - p1_move_to) <= player1_dice[1] or (p1_move_from - p1_move_to) <= (player1_dice[0] + player1_dice[1])) and p1_move_to == 0):
                        decPlayer1(p1_move_from)
                    else:
                        print("Move is invalid")

                    print("Dices: {}".format(player1_dice))
                    print("Moves: {}, {}".format(p1_move_from, p1_move_to))
                    print("Table player 1: {}".format(table_player1))
                    print("#########################")

            # Moves for player 2
            if player2_moves.strip():
                p2_moves_set = [x for x in player2_moves.split()]
                for i in range(len(p2_moves_set)):
                    if 'Bar' in p2_moves_set[i].split('/')[0]:
                        p2_move_from = 25
                    else:
                        p2_move_from = int(p2_moves_set[i].split('/')[0])

                    if '*' in p2_moves_set[i].split('/')[1]:
                        p2_move_to = int(p2_moves_set[i].split('/')[1][:-1])
                        table_player1[24 - p2_move_to][1] -= 1
                    else:
                        p2_move_to = int(p2_moves_set[i].split('/')[1])

                    if(main.isDiceMoveValidForOne(player2_dice[0], p2_move_to, p2_move_from) == True):
                        errorMove = False
                    elif(main.isDiceMoveValidForOne(player2_dice[1], p2_move_to, p2_move_from) == True):
                        errorMove = False
                    elif(main.isDiceMoveValidForSum(player2_dice, p2_move_to, p2_move_from) == True):
                        errorMove = False
                    elif (((p2_move_from - p2_move_to) <= player2_dice[0] or (p2_move_from - p2_move_to) <= player2_dice[1] or (p2_move_from - p2_move_to) <= (player2_dice[0] + player2_dice[1])) and p2_move_to == 0):
                        # decPlayer2(p2_move_from)
                        errorMove = False
                    else:
                        errorMove = True
                        print("Invalid move")
                        exit(0)

                    if(main.checkPosibleMove(25 - p2_move_to, table_player1) == True):
                        print("Move is valid")
                        if(p2_move_from != 25 and p2_move_to):
                            addPlayer2(p2_move_to)
                            decPlayer2(p2_move_from)
                        elif(p2_move_from != 25 and p2_move_to == 0):
                            decPlayer2(p2_move_from)
                        elif(p2_move_from == 25 and p2_move_to):
                            addPlayer2(p2_move_to)
                        elif(p2_move_from == 25 and p2_move_to == 0):
                            print("Error")
                            exit(0)
                    elif (((p2_move_from - p2_move_to) <= player2_dice[0] or (p2_move_from - p2_move_to) <= player2_dice[1] or (p2_move_from - p2_move_to) <= (player2_dice[0] + player2_dice[1])) and p2_move_to == 0):
                        decPlayer2(p2_move_from)
                    else:
                        print("Move is invalid")

                    print("Dices: {}".format(player2_dice))
                    print("Moves: {}, {}".format(p2_move_from, p2_move_to))
                    print("Table player 2: {}".format(table_player2))
                    print("#########################")

                    gui_test.initializeBoard(table_player1, table_player2)
                    # time.sleep(1)


parse(rounds_filepath)
