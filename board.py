from turtle import Turtle, Screen
from tkinter import *

def drawBoard():
    board_width = 1000
    board_height = 600
    board_rectangles = board_width//15

    board_frame=Frame(width=board_width+100, height=board_height+100, relief=SUNKEN, bd=1)
    board_frame.pack()
    
    board = Canvas(board_frame,width=board_width,height=board_height,background="#ffffff")
    board.pack()
    #Set up board graphics
    #Edges
    board.create_rectangle(0,0,board_rectangles,board_height,fill="black")
    board.create_rectangle(14*board_rectangles,0,board_width,board_height,fill="black")
    #Middle
    board.create_rectangle(7*board_rectangles,0,8*board_rectangles,7*board_height//15,fill="black")
    board.create_rectangle(7*board_rectangles,8*board_height//15,8*board_rectangles,board_height,fill="black")
    #Triangles
    for i in range(1,14):
        if(i == 7):
            continue
        board.create_polygon(i*board_rectangles,0,(i+1)*board_rectangles,0,((2*i+1)*board_rectangles)//2,6*board_height//15,fill="#332324")
        board.create_polygon(i*board_rectangles,board_height,(i+1)*board_rectangles,board_height,((2*i+1)*board_rectangles)//2,9*board_height//15,fill="#332324")

    #save img
    board.update()
    board.postscript(file="./imgs/capture1.png", colormode='color')

def main():
    root=Tk()
    drawBoard()   
    root.mainloop()

if __name__ == "__main__":
    main()