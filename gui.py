import pygame

class Points_Gui:

    def __init__(self):
        #triangles position
        self.x_positions = [65, 134, 200, 268, 338, 408, 531, 599, 667, 735, 803, 871]
        #distance piece-border(up)
        self.bench = 5
        #distance piece_border(down)
        self.far_bench = 600

    def get_position(self, position):
        # index=0
        # height=1
        index, height = position[0] - 1, position[1]
        if index >= 12:
            x_pos = self.x_positions[23-index]
            y_pos = self.far_bench - (55 * height)
        else:
            x_pos = self.x_positions[index]
            y_pos = self.bench + (55 * (height - 1))

        return x_pos, y_pos

class Gui:
    def __init__(self):
        pygame.display.set_caption("PBR Project - Backgammon")
        #creating a surface
        self.board_img = pygame.image.load("./imgs/capture.png")
        #sets the window size => width=980, height=480
        self.screen = pygame.display.set_mode((980, 600))
        #draws the board surface at position x=0, y=0
        self.screen.blit(self.board_img, (0, 0))
        #updates the screen
        pygame.display.flip()
        self.white_piece = pygame.image.load(
            "./imgs/white.png")
        self.black_piece = pygame.image.load(
            "./imgs/black.png")
        self.point_coords = Points_Gui()

    def get_piece_color(self, player_color):
        if player_color == "White":
            return self.white_piece
        if player_color == "Black":
            return self.black_piece

    def place_piece(self, player_color, position):
        piece_img = self.get_piece_color(player_color)
        piece_coordinates = self.point_coords.get_position(position)
        #rect represents the region of the subsurface within the original surface
        piece_rect = self.screen.blit(piece_img, piece_coordinates)
        pygame.display.update(piece_rect)

    def remove(self, position):
        piece_coordinates = self.point_coords.get_position(position)
        piece_rect = pygame.Rect(piece_coordinates, (55, 55))
        self.screen.blit(self.board_img, piece_rect, piece_rect)
        pygame.display.update(piece_rect)

    def move(self, player_color, start_position, end_position):
        self.remove(start_position)
        self.place_piece(player_color, end_position)