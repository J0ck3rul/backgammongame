checkPosition(X, [X|_]).
checkPosition(X, [Y|_]) :- [_,Yx] = Y, Yx =< 1, checkPosition(X, Y).
checkPosition(X, [_|Ys]) :- checkPosition(X, Ys).

% list of elements (list of list)
% in what position we want to insert (X)
% example: checkPosition(15, [[12,0],[15,3],[14,2],[1,1]])

checkDiceMoveOne(Z, A, B) :-
  Zrez is Z,
  Dist is (B-A),
  Zrez = Dist.

checkDiceMoveSum(Z, A, B) :- 
  [Z1, Z2] = Z,
  Zrez is (Z1+Z2),
  Dist is (B-A),
  Zrez = Dist.

% Z is a [] of dice elements,
% A is the start position,
% B is the final position